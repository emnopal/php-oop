<?php

class Ape extends Animal
{
    private string $yell;

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->legs = 2;
        parent::__construct(name: $this->name, legs: $this->legs);

        $this->yell = "Auooo";
    }

    public function yell(): string
    {
        return "Yell: $this->yell" . "<br>";
    }
}
