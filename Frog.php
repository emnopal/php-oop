<?php

class Frog extends Animal
{
    private string $jump;

    public function __construct(string $name)
    {
        $this->name = $name;
        parent::__construct(name: $this->name);

        $this->jump = "Hop Hop";
    }

    public function jump(): string
    {
        return "Jump: $this->jump" . "<br>";
    }
}
