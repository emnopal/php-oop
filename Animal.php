<?php

class Animal
{
    protected string $name;
    protected int|string|null $legs;
    protected ?string $cold_blooded;

    public function __construct(string $name, int|string|null $legs = null, ?string $cold_blooded = null)
    {
        $this->name = $name;
        if (!$legs) {
            $this->legs = 4;
        } if (!$cold_blooded) {
            $this->cold_blooded = "no";
        } else {
            $this->legs = $legs;
            $this->cold_blooded = $cold_blooded;
        }
    }

    public function getName(): string
    {
        return "Name: " . $this->name . "<br>";
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getLegs(): string
    {
        return "Legs: " . $this->legs . "<br>";
    }

    public function setLegs(int|string $legs): void
    {
        $this->legs = $legs;
    }

    public function getColdBlooded(): string
    {
        return "Cold Blooded: " . $this->cold_blooded . "<br>";
    }

    public function setColdBlooded(string $cold_blooded): void
    {
        $this->cold_blooded = $cold_blooded;
    }
}