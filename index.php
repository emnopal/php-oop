<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP OOP</title>
</head>

<body>
<?php

require_once __DIR__ . "/Animal.php";
require_once __DIR__ . "/Frog.php";
require_once __DIR__ . "/Ape.php";

$animal = new Animal("shaun");
echo $animal->getName(); // shaun
echo $animal->getLegs(); // 4
echo $animal->getColdBlooded(); // no

echo "<br>";

$frog = new Frog("buduk");
echo $frog->getName(); // buduk
echo $frog->getLegs(); // 4
echo $frog->getColdBlooded(); // no
echo $frog->jump(); // Hop Hop

echo "<br>";

$ape = new Ape("kera sakti");
echo $ape->getName(); // kera sakti
echo $ape->getLegs(); // 2
echo $ape->getColdBlooded(); // no
echo $ape->yell(); // Auooo

?>
</body>

